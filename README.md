# CKutils

This is an R package with some utility functions I use regularly.

You can install it using


```R
library(remotes)
remotes::install_bitbucket("ChristK/CKutils")
```
