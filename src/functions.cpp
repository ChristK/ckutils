/* CKutils: an R package with some utility functions I use regularly
Copyright (C) 2018  Chris Kypridemos

CKutils is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>
or write to the Free Software Foundation, Inc., 51 Franklin Street,
Fifth Floor, Boston, MA 02110-1301  USA.*/

// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <random> // for rng to work
#include "pcg_random.h" // from http://www.pcg-random.org implementing pcg rng
//#include <string>
//#include <cmath>
using namespace Rcpp;

//quantile implementation for default R method (type 7)
//' @export
// [[Rcpp::export]]
NumericVector fquantile(NumericVector x, NumericVector probs, bool na_rm = true) {
  if (all(is_na(x))) {
    NumericVector out(probs.size(), NA_REAL);
    return(out);
  }
  if (na_rm) x = na_omit(x);
  const int n = x.size();
  NumericVector out(probs.size());
  IntegerVector ii(probs.size());
  NumericVector h(probs.size());
  NumericVector index = 1 + (n - 1) * probs;
  NumericVector lo = floor(index); //floor
  //ceiling
  NumericVector hi = ceiling(index);
  for(int i = 0; i < probs.size(); i++)
  {//catch corner case when index element is int and ceiling = floor
    h[i] = index[i] - lo[i];
  }
  std::sort(x.begin(), x.end());
  out = x[as<IntegerVector>(lo) - 1];
  if (all(is_na(ii)))
  {
    return(out);
  } else
  {
    x = x[as<IntegerVector>(hi) - 1];
    for(int i = 0; i < probs.size(); i++)
    {
      out[i] = (1 - h[i]) * out[i] + h[i] * x[i];
    }
    return(out);
  }
}

//' @export
// [[Rcpp::export]]
int count_if(LogicalVector x, bool na_rm = false) {
  if (na_rm) x = na_omit(x); // remove NA from denominator
  const int n = x.size();
  int counter = 0;
  for(int i = 0; i < n; i++) {
    if(x[i] == TRUE) {
      counter++;
    }
  }
  return counter;
}

//' @export
// [[Rcpp::export]]
double prop_if(LogicalVector x, bool na_rm = false) {
  if (na_rm) x = na_omit(x); // remove NA from denominator
  const int n = x.size();
  int counter = 0;
  for(int i = 0; i < n; i++) {
    if(x[i] == TRUE) {
      counter++;
    }
  }
  return counter/(double)n;
}

// helper function implementing a uniform rng using pcg algo by M.E. O'Neill
// from http://www.pcg-random.org/
// set seed for rng
pcg_extras::seed_seq_from<std::random_device> seed_source;
// make a random number engine to be used for my_runif()
pcg32 rng(seed_source);

double my_runif(const double lower_bound, const double upper_bound)
{
  // Choose a random number between lower_bound and upper_bound
  std::uniform_real_distribution<double> uniform_dist(lower_bound, upper_bound);
  return uniform_dist(rng);
}

// helper function that makes a double jump within x - jump and x + jump
// special care to avoid values outside (0, 1)
double fscramble_hlp (const double x, const double jump)
{
  double out = my_runif(x - jump, x + jump);
  if ((out <= 0.0) | (out >= 1.0)) out = x;

  return out;
}

//' @export
// [[Rcpp::export]]
NumericVector fscramble_trajectories(const NumericVector x, const IntegerVector pid, const double jump) {
  // pid should be sorted and same length as x
  int n = x.size();
  NumericVector out(n);
  out[0] = x[0];
  for (int i = 1; i < n; ++i)
  {
    if (pid[i] == pid[i-1])
    {
      out[i] = fscramble_hlp(out[i-1], jump);
    }
    else
    {
      out[i] = x[i];
    }
  }
  return out;
}
